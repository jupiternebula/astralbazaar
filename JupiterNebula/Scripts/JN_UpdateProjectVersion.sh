#!/bin/sh

if [ -f JN_GitProjVer.awk ]; then
    # Needed to make Git happy
    GIT_DIR_BAK="$GIT_DIR"
    unset GIT_DIR

    # Platforms to set the version for
    PLATFORMS=( Windows Mac IOS Android PS4 XboxOne HTML5 Linux )
    # Used to ensure proper paths and to check if we are in a git repo
    CFGPATH="`git rev-parse --show-cdup 2> /dev/null`Saved/Config"

    if [ $? -ne 0 ]; then
        echo "!JN_HOOK: Cannot update project version outside of a Git repo!"
        GIT_DIR="$GIT_DIR_BAK"
        exit
    fi

    echo "@JN_HOOK: Updating Saved/ Unreal project version from Git commit..."

    for i in "${PLATFORMS[@]}"
    do
        mkdir -p "$CFGPATH/$i"

        if [ ! -f "$CFGPATH/$i/Game.ini" ]; then
            touch "$CFGPATH/$i/Game.ini"
        fi

        cat "$CFGPATH/$i/Game.ini" | awk -f ./JN_GitProjVer.awk > _Game.ini
        cp _Game.ini "$CFGPATH/$i/Game.ini"
    done

    rm _Game.ini
    GIT_DIR="$GIT_DIR_BAK"
else
    # This script depends on JN_GitProjVer.awk
    echo "!JN_HOOK: Cannot find JN_GitProjVer.awk! Aborting..."
fi