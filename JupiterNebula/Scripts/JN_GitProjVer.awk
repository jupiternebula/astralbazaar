#!/usr/bin/awk -f

BEGIN {
    # Ensure CR+LF line endings
    ORS = "\r\n"

    # Get the current git describe string
    gitDescCmd = "git describe 2> /dev/null"
    gitDescCmd | getline gitDesc
    close(gitDescCmd)

    # When needsProjVerAppended is > 0 then a ProjectVersion value will
    # be appended to the end of the file (during END)
    if (gitDesc != "")
    {
        needsProjVerAppended = 1
    }
    else
    {
        needsProjVerAppended = 0
    }
}

{
    # Tokenize on the "=" separator
    split($0, tokens, "=", seps)

    # If we are in a Git directory and we are on a ProjectVersion line,
    # then set the ProjectVersion string to gitDesc (set in BEGIN)
    if (gitDesc != "" && tokens[1] == "ProjectVersion")
    {
        print tokens[1] seps[1] gitDesc
        needsProjVerAppended = 0
    }
    else
    {
        print $0
    }
}

# Append a ProjectVersion key value pair to the end of the file if
# needsProjVerAppended was set to a value greater than 0
END {
    if (needsProjVerAppended > 0)
    {
        print ""
        print "[/Script/EngineSettings.GeneralProjectSettings]"
        print "ProjectVersion=" gitDesc
    }
}