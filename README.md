# Astral Bazaar

Astral Bazaar is a work-in-progress, 3D real-time strategy game written in C++ using Unreal Engine 4. The game focuses on the economics of operating a
large conglomerate in the 23rd century across the Sol system. Players can conduct mining operations, explore the asteroid belt, create factories to refine
ore into finished products, and much more in a virtual sandbox of the future solar system.

## Video
[Earth Material Preview](https://youtu.be/-qGL1E9_0-E)

[First Two Days of Progress](https://youtu.be/dSoofCDq1mk)

## Screenshots

All screenshots can be viewed on [Imgur](https://imgur.com/a/B8Vkz)

![Earth Screenshot](https://i.imgur.com/Es0ysvz.jpg)
![Saturn Rings Screenshot](https://i.imgur.com/EkSmQLY.jpg)
![Solar Neighborhood Screenshot](https://i.imgur.com/btJJQhP.jpg)
![Uranus Screenshot](https://i.imgur.com/p48EDMh.jpg)

## Repository Structure

Astral Bazaar was initially intended to be released as a commercial game, but the author ran out of time to work on the project. As such, the source
has been made public along with all of its assets in Unreal Engine 4's asset format. The sources are available under the "Source" directory, all
assets are located under "Content", and all utility scripts used for developers of the game are located under "JupiterNebula/Scripts"

## Files of Interest

Unfortunately, much of the source has yet to be commented or documented. The primary sources and headers that may be useful are as follows:

    OrbitalBody.{cpp,h}
	Body.{cpp,h}
	SplineVisualizer.{cpp,h}
	OverviewPawn.{cpp,h}

Some assets that may be of interest (requires Unreal Engine 4 to view):

    /Content/Planets/Common/M_Planet.uasset
	/Content/HUD/Widgets/TreeView/*.uasset
	/Content/Core/MaterialFunctions/MF_*.uasset

Note that the TreeView widget assets above may be released as a separate package on the Unreal Engine marketplace at some point in the future.

## License

Astral Bazaar and its assets not attributed to other copyright owners are Copyright (C) 2017 Joshua William Taylor. The author assumes no responsibility
for any damages resulting from running this software. Any third-party use of code or assets contained in this repository are subject to the original
licenses of those assets if applicable, and/or attribution to the respective author(s) / licensor(s).

I would ask that anyone interested in using this code for self-learning, education, commercial purposes, or any other reason contacts me at
[taylor.joshua88@gmail.com](mailto:taylor.joshua88@gmail.com)

## Developers
Please add the following to the bottom of the ".git/hooks/post-checkout" and ".git/hooks/post-commit" hooks:

    # Update Unreal project version string in "Saved/Windows"
    cd JupiterNebula/Scripts
    ./JN_UpdateProjectVersion.sh