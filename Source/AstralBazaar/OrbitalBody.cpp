// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#include "AstralBazaar.h"
#include "Components/SplineComponent.h"
#include "Engine.h"
#include "OrbitalBody.h"

AOrbitalBody::AOrbitalBody()
{
	PrimaryActorTick.bCanEverTick = true;

	OrbitSpline = CreateDefaultSubobject<USplineComponent>(TEXT("OrbitSpline"));
	OrbitSpline->SetupAttachment(RootComponent);
	OrbitSpline->SetBoundsScale(0.f);

	bIsInOrbit = true;
	NumSplinePoints = 23;
}

void AOrbitalBody::BeginPlay()
{
	if (IsValid(OrbitSpline))
	{
		SnapOrbitSpline();
		GenerateOrbitSplinePoints();
	}

	Super::BeginPlay();
}

void AOrbitalBody::GenerateOrbitSplinePoints()
{
	if (!IsValid(OrbitSpline))
		return;

	const float PhaseToRadFactor = (2.f * PI) / (NumSplinePoints + 1);
	const float TrueRadius = (OrbitalParameters.Apoapsis + OrbitalParameters.Periapsis) / 2.f;
	const float TrueCenterOffset = (OrbitalParameters.Apoapsis - OrbitalParameters.Periapsis) / 2.f;
	const float EccentricityFactor = FMath::Sqrt(FMath::Pow(OrbitalParameters.Periapsis * 2.f, 2.f) - FMath::Pow(TrueRadius * 2.f - OrbitalParameters.Periapsis * 2.f, 2.f)) / 2.f;
	FVector PredictionVector{ 0.f, 0.f, 0.f };

	OrbitalParameters.Validate();
	OrbitSpline->ClearSplinePoints();
	OrbitSpline->Duration = OrbitalParameters.CycleDuration;

	// TODO: Find a better way to figure the best number of spline points
	for (int32 i = 0.f; i <= NumSplinePoints; i += 1.f)
	{
		FMath::SinCos(&(PredictionVector.Y), &(PredictionVector.X), i * PhaseToRadFactor);
		PredictionVector.X = PredictionVector.X * TrueRadius + TrueCenterOffset;
		PredictionVector.Y *= EccentricityFactor;

		OrbitSpline->AddSplinePoint(PredictionVector.RotateAngleAxis(OrbitalParameters.Inclination, FVector(0.f, 1.f, 0.f)), ESplineCoordinateSpace::Local, false);
	}

	// Set closed loop and update the spline (true to second parameter)
	OrbitSpline->SetClosedLoop(true, true);
}

void AOrbitalBody::SnapOrbitSpline()
{
	if (IsValid(OrbitSpline))
	{
		AActor* AttachParentActor = GetAttachParentActor();

		if (IsValid(AttachParentActor))
		{
			TEnumAsByte<EComponentMobility::Type> AttachParentMobility = AttachParentActor->GetRootComponent()->Mobility;

			if (AttachParentMobility != OrbitSpline->Mobility)
			{
					OrbitSpline->DetachFromParent(true, false);
					OrbitSpline->SetMobility(AttachParentMobility);
			}

			OrbitSpline->AttachToComponent(AttachParentActor->GetRootComponent(), FAttachmentTransformRules::SnapToTargetIncludingScale);
		}
		else if (IsValid(OrbitSpline->GetAttachParent()))
		{
			OrbitSpline->DetachFromParent(false, false);
		}
	}
}

// HACK: Probably redundant to call reconstruct the orbital spline in OnConstruction and PostInitializeComponents
bool AOrbitalBody::Modify(bool bAlwaysMarkDirty)
{
	SnapOrbitSpline();
	GenerateOrbitSplinePoints();

	return Super::Modify(bAlwaysMarkDirty);
}

void AOrbitalBody::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (IsValid(OrbitSpline))
	{
		SnapOrbitSpline();
		GenerateOrbitSplinePoints();
	}
}

void AOrbitalBody::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
}

#if WITH_EDITOR
void AOrbitalBody::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	if (PropertyChangedEvent.PropertyChain.GetHead()->GetValue()->GetFName() == GET_MEMBER_NAME_CHECKED(AOrbitalBody, OrbitalParameters))
	{
		OrbitalParameters.Validate();
		SnapOrbitSpline();
		GenerateOrbitSplinePoints();

		if (IsValid(OrbitSpline))
			OrbitSpline->Duration = OrbitalParameters.CycleDuration;
	}

	Super::PostEditChangeChainProperty(PropertyChangedEvent);
}

void AOrbitalBody::PostEditMove(bool bFinished)
{
	if (bFinished)
	{
		SnapOrbitSpline();
		GenerateOrbitSplinePoints();
	}

	return Super::PostEditMove(bFinished);
}
#endif

float AOrbitalBody::GetSortingDistanceSquared() const
{
	if (bIsInOrbit)
		return FMath::Square(OrbitalParameters.Periapsis);
	else
		return Super::GetSortingDistanceSquared();
}

float AOrbitalBody::PredictPhase(float DeltaTime) const
{
	if (FMath::IsNearlyZero(DeltaTime))
		return OrbitalParameters.Phase;
	return bIsInOrbit ? FMath::Fmod(OrbitalParameters.Phase + DeltaTime, OrbitalParameters.CycleDuration) : OrbitalParameters.Phase;
}

FVector AOrbitalBody::PredictRelativeLocation(float DeltaTime)
{
	SnapOrbitSpline();

	if (bIsInOrbit && IsValid(OrbitSpline))
		return OrbitSpline->GetLocationAtTime(PredictPhase(DeltaTime), ESplineCoordinateSpace::Local);
	else
		return GetActorLocation();
}

FTransform AOrbitalBody::PredictRelativeTransform(float DeltaTime)
{
	SnapOrbitSpline();

	if (bIsInOrbit && IsValid(OrbitSpline))
		return OrbitSpline->GetTransformAtTime(PredictPhase(DeltaTime), ESplineCoordinateSpace::Local);
	else
		return Super::PredictTransform(DeltaTime);
}

void AOrbitalBody::AdvanceSimulation(float DeltaTime)
{
	Super::AdvanceSimulation(DeltaTime);

	// TODO: Eliminate the extraneous FMod call by predicting phase twice in each frame
	OrbitalParameters.Phase = PredictPhase(DeltaTime);
}