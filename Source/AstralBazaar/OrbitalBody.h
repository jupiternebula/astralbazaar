// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "Body.h"
#include "OrbitalBody.generated.h"

USTRUCT(BlueprintType)
struct FOrbitalParameters
{
	GENERATED_BODY()

	// The farthest point of the orbit relative to the attach parent
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters)
	float Apoapsis;
	// The nearest point of the orbit relative to the attach parent
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters)
	float Periapsis;
	// The pitch in degrees of the orbital path relative to the forward vector of the attach parent
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters, meta = (ClampMin = -180.0, ClampMax = 180.0))
	float Inclination;
	// The amount of time in Earth days that it takes to complete one full orbit
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters, meta = (ClampMin = 0.0001))
	float CycleDuration;
	// The current position along the orbit path in Earth days starting from periapsis TODO: Confirm that this is the case
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters)
	float Phase;

	// Ensure that the provided parameters are within the correct ranges.
	void Validate()
	{
		Apoapsis = FMath::Max(Apoapsis, KINDA_SMALL_NUMBER);
		Periapsis = FMath::Clamp<float>(Periapsis, Apoapsis / 3.f, Apoapsis);
		Inclination = FMath::UnwindDegrees(Inclination);
		CycleDuration = FMath::Max(CycleDuration, KINDA_SMALL_NUMBER);
		
		while (Phase < 0.f)
		{
			Phase += CycleDuration;
		}
		Phase = FMath::Fmod(Phase, CycleDuration);
	}

	FOrbitalParameters()
	{
		Apoapsis = 1000.f;
		Periapsis = 750.f;
		Inclination = 0.f;
		CycleDuration = 15.f;
		Phase = 0.f;
	}
};

UCLASS(Blueprintable)
class ASTRALBAZAAR_API AOrbitalBody : public ABody
{
	GENERATED_BODY()

public:
	AOrbitalBody();

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = OrbitalBody)
	class USplineComponent* OrbitSpline;

protected:
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = OrbitalParameters, meta = (ShowOnlyInnerProperties))
	FOrbitalParameters OrbitalParameters;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters, meta = (ClampMin = 3))
	int32 NumSplinePoints;

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters)
	uint8 bIsInOrbit : 1;

	void GenerateOrbitSplinePoints();
	void SnapOrbitSpline();

public:
	virtual bool Modify(bool bAlwaysMarkDirty = true) override;
	virtual void PostInitializeComponents() override;
	virtual void Tick(float DeltaTime) override;

#if WITH_EDITOR
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent) override;
	virtual void PostEditMove(bool bFinished) override;
#endif

	virtual float GetSortingDistanceSquared() const override;

	UFUNCTION(BlueprintPure, Category = OrbitalBody)
	FORCEINLINE float PredictPhase(float DeltaTime) const;

	virtual FVector PredictRelativeLocation(float DeltaTime) override;
	virtual FTransform PredictRelativeTransform(float DeltaTime) override;
	virtual void AdvanceSimulation(float DeltaTime) override;
};
