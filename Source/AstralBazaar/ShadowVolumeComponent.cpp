// Copyright (C) 2017 Joshua William Taylor. All rights reserved.

#include "AstralBazaar.h"
#include "DrawDebugHelpers.h"
#include "ShadowVolumeComponent.h"

UShadowVolumeComponent::UShadowVolumeComponent()
{
	bGenerateOverlapEvents = true;
	OnComponentBeginOverlap.AddDynamic(this, &UShadowVolumeComponent::OnOverlapBegin);
	OnComponentEndOverlap.AddDynamic(this, &UShadowVolumeComponent::OnOverlapEnd);
	ShadowCasterName = FName(TEXT("ShadowCaster0"));
	ShadowCasterRadius = 50.f;
	SphereRadius = 50.f;

	PrimaryComponentTick.bCanEverTick = true;
	PrimaryComponentTick.bStartWithTickEnabled = true;
	PrimaryComponentTick.TickGroup = TG_PostUpdateWork;
	bAutoActivate = true;
	bAutoRegister = true;
	SetMobility(EComponentMobility::Movable);
	bUseAttachParentBound = true;

	bShowDebugHelpers = false;
	
	return;
}

bool UShadowVolumeComponent::ManageMIDs(AActor* ActorIn)
{
	if (ManagedMIDs.Contains(ActorIn))
		return false;

	for (UActorComponent* CurActorComponent : ActorIn->GetComponentsByClass(UPrimitiveComponent::StaticClass()))
	{
		UPrimitiveComponent* CurPrimitiveComponent = Cast<UPrimitiveComponent>(CurActorComponent);
		if (CurPrimitiveComponent != nullptr)
		{
			for (int32 i = 0; i < CurPrimitiveComponent->GetNumMaterials(); i++)
			{
				UMaterialInstanceDynamic* CurMID = Cast<UMaterialInstanceDynamic>(CurPrimitiveComponent->GetMaterial(i));
				// TODO: Add check to see if the MID even checks for shadow casters

				if (CurMID != nullptr)
				{
					ManagedMIDs.Add(ActorIn, CurMID);
				}
			}
		}
	}

	return true;
}

bool UShadowVolumeComponent::UnmanageMIDs(AActor* ActorIn)
{
	if (!ManagedMIDs.Contains(ActorIn))
		return false;

	TArray<UMaterialInstanceDynamic*> ActorMIDArray;
	ManagedMIDs.MultiFind(ActorIn, ActorMIDArray, false);
	for (UMaterialInstanceDynamic* CurMID : ActorMIDArray)
	{
		CurMID->SetVectorParameterValue(ShadowCasterName, FLinearColor(0.f, 0.f, 0.f, 0.f));
	}

	return (ManagedMIDs.Remove(ActorIn) > 0 ? true : false);
}

FLinearColor UShadowVolumeComponent::GetShadowCasterLinearColor()
{
	FVector ShadowVolumeLocation = GetComponentLocation();

	return FLinearColor(ShadowVolumeLocation.X, ShadowVolumeLocation.Y, ShadowVolumeLocation.Z, ShadowCasterRadius);
}

void UShadowVolumeComponent::OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
	int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult)
{
	if (bShowDebugHelpers && (OtherActor != nullptr) && (OtherActor != this->GetOwner()) && (OtherComp != nullptr))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, OtherActor->GetName() + TEXT(" -> ") + this->GetName());
		DrawDebugLine(GetWorld(), GetComponentLocation(), OtherActor->GetActorLocation(), FColor::Turquoise, false, 5.f);

		ManageMIDs(OtherActor);
	}
	if (bShowDebugHelpers)
		GEngine->AddOnScreenDebugMessage(-1, 10.f, FColor::Magenta, GetOwner()->GetActorLabel() + TEXT(" manages MIDS for: ") + FString::FromInt(ManagedMIDs.Num()));
}

void UShadowVolumeComponent::OnOverlapEnd(UPrimitiveComponent * OverlappedComp, AActor * OtherActor, UPrimitiveComponent * OtherComp, int32 OtherBodyIndex)
{
	if (bShowDebugHelpers && (OtherActor != nullptr) && (OtherActor != this->GetOwner()) && (OtherComp != nullptr))
	{
		GEngine->AddOnScreenDebugMessage(-1, 5.f, FColor::Yellow, OtherActor->GetName() + TEXT(" !-> ") + this->GetName());
		DrawDebugLine(GetWorld(), GetComponentLocation(), OtherActor->GetActorLocation(), FColor::Purple, false, 5.f);

		UnmanageMIDs(OtherActor);
	}
}

void UShadowVolumeComponent::OnRegister()
{
	Super::OnRegister();

	if (IsValid(GetAttachParent()))
	{
		ShadowCasterRadius = GetAttachParent()->Bounds.GetSphere().W;
		SphereRadius = ShadowCasterRadius / GetAttachParent()->GetComponentScale().X;
	}
}

void UShadowVolumeComponent::BeginPlay()
{
	Super::BeginPlay();

	TSet<AActor*> OverlappingActors;
	GetOverlappingActors(OverlappingActors);
	
	for (AActor* OverlappingActor : OverlappingActors)
	{
		if (OverlappingActor != nullptr && OverlappingActor != this->GetOwner())
			ManageMIDs(OverlappingActor);
	}
}

void UShadowVolumeComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);
	FLinearColor FrameLinearColor = GetShadowCasterLinearColor();

	for (auto& CurMIDPair : ManagedMIDs)
	{
		CurMIDPair.Value->SetVectorParameterValue(ShadowCasterName, FrameLinearColor);
	}
}
