// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#include "AstralBazaar.h"
#include "Selectable.h"


// This function does not need to be modified.
USelectable::USelectable(const class FObjectInitializer& ObjectInitializer)
: Super(ObjectInitializer)
{
}

// Add default functionality here for any ISelectable functions that are not pure virtual.
