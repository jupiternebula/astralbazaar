// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#include "AstralBazaar.h"
#include "AstralBazaarPlayerControllerBase.h"
#include "Selectable.h"
#include "Body.h"
#include "OverviewPawn.h"


// Sets default values
AOverviewPawn::AOverviewPawn()
{
	PrimaryActorTick.bCanEverTick = true;
	bReplicates = false;

	// Root scene node
	RootSceneComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));
	RootComponent = RootSceneComponent;

	// Camera
	CameraComponent = CreateDefaultSubobject<UCameraComponent>(TEXT("CameraComponent"));
	CameraComponent->SetupAttachment(RootComponent);
	CameraComponent->bUsePawnControlRotation = true;

	// Floating pawn movement component
	MovementComponent = CreateDefaultSubobject<UFloatingPawnMovement>(TEXT("MovementComponent"));

	// Pawn / actor properties
	BaseEyeHeight = 0.f;
	bCollideWhenPlacing = false;
	SpawnCollisionHandlingMethod = ESpawnActorCollisionHandlingMethod::AlwaysSpawn;
	bUseControllerRotationPitch = false;
	bUseControllerRotationRoll = false;
	bUseControllerRotationYaw = true;

	// OverviewPawn-specific properties
	InputContext = EInputContext::MOUSE_AND_KEYBOARD;

	ZHeightScalingBaseline = 3500.f;

	PendingZoom = 0.f;
	ZoomInterpSpeed = 8.0f;
	ZoomScale = 0.01f;

	PanSpeedScale = 1.85f;
	PanAccelerationScale = 1.5f;
	PanDecelerationScale = 6.54f;

	MouseYawFactor = 2.875f;
	MousePitchFactor = 1.875f;
	RotationMousePositionX = 0.f;
	RotationMousePositionY = 0.f;
	AdjustMovementComponent();

	StoredWorldTimeDilation = 1.f;
}

void AOverviewPawn::AdjustMovementComponent()
{
	float ActorZ = FMath::Max(FMath::Abs(GetActorLocation().Z), 2500.f);
	MovementComponent->MaxSpeed     = ActorZ * PanSpeedScale,
	MovementComponent->Acceleration = ActorZ * PanAccelerationScale;
	MovementComponent->Deceleration = ActorZ * PanDecelerationScale;
	
	/*
	MovementComponent->MaxSpeed = CameraComponent->GetComponentLocation().Z * PanSpeedScale;
	MovementComponent->Acceleration = CameraComponent->GetComponentLocation().Z * PanAccelerationScale;
	MovementComponent->Deceleration = CameraComponent->GetComponentLocation().Z * PanDecelerationScale;
	*/
}

// Called when the game starts or when spawned
void AOverviewPawn::BeginPlay()
{
	Super::BeginPlay();

	StoredWorldTimeDilation = GetWorldSettings()->TimeDilation; // TODO: Add a check for GetWorldSettings
	CustomTimeDilation = 1.f / StoredWorldTimeDilation;

	ZoomDirection = CameraComponent->GetForwardVector();
	AdjustMovementComponent();
}

void AOverviewPawn::PossessedBy(class AController* NewController)
{
	if (bReplicates)
	{
		Super::PossessedBy(NewController);
	}
	else
	{
		AController* const OldController = Controller;
		Controller = NewController;

		if (OldController != NewController)
		{
			ReceivePossessed(Controller);
		}
	}

	PlayerController = Cast<AAB_PlayerControllerBase>(Controller);

	if (!PlayerController)
	{
		UE_LOG(LogTemp, Warning, TEXT("OverviewPawn possessed by a non-player Controller actor!"));
	}
}

// Called every frame
void AOverviewPawn::Tick(float DeltaTime)
{
	// Make sure the actor is not affected by global time dilation
	if (StoredWorldTimeDilation != GetWorldSettings()->TimeDilation)
	{
		StoredWorldTimeDilation = GetWorldSettings()->TimeDilation;
		CustomTimeDilation = 1.f / StoredWorldTimeDilation;
	}

	Super::Tick(DeltaTime);

	/*
	SpringArmComponent->TargetArmLength = FMath::FInterpTo(SpringArmComponent->TargetArmLength,
		FMath::GetMappedRangeValue(FVector2D(0.f, 1.f), FVector2D(ZoomMinimum, ZoomMaximum), DesiredZoom), DeltaTime, ZoomInterpSpeed);
	*/
	/*
	SpringArmComponent->TargetArmLength = FMath::FInterpTo(SpringArmComponent->TargetArmLength,
		FMath::InterpEaseIn<float>(ZoomMinimum, ZoomMaximum, DesiredZoom, 4.f), DeltaTime, ZoomInterpSpeed);
	PlayerController->PlayerCameraManager->ViewPitchMax = FMath::InterpEaseOut<float>(15.f, -89.900002, DesiredZoom, ZoomPitchExponent);
	SpringArmComponent->SetRelativeRotation(FRotator(GetControlRotation().Pitch, 0.f, 0.f));
	*/

	AdjustMovementComponent();

	if (!FMath::IsNearlyZero(PendingZoom))
	{
		GEngine->AddOnScreenDebugMessage(-1, 0.f, FColor::Purple, FString::SanitizeFloat(PendingZoom));
		AddActorWorldOffset(ZoomDirection * PendingZoom * MovementComponent->MaxSpeed * ZoomScale);
		PendingZoom = FMath::FInterpTo(PendingZoom, 0.f, DeltaTime, ZoomInterpSpeed);
	}

	// Maintain cursor position during rotation
	if (bFocusPressed && IsValid(PlayerController))
	{
		PlayerController->SetMouseLocation(RotationMousePositionX, RotationMousePositionY);
	}
}

// Called to bind functionality to input
void AOverviewPawn::SetupPlayerInputComponent(UInputComponent* PlayerInputComponent)
{
	Super::SetupPlayerInputComponent(PlayerInputComponent);

	InputComponent->BindAction(TEXT("Pick"), IE_Pressed, this, &AOverviewPawn::OnPick_Pressed);
	InputComponent->BindAction(TEXT("Pick"), IE_Released, this, &AOverviewPawn::OnPick_Released);

	InputComponent->BindAction(TEXT("ModPick"), IE_Pressed, this, &AOverviewPawn::OnModPick_Pressed);
	InputComponent->BindAction(TEXT("ModPick"), IE_Released, this, &AOverviewPawn::OnModPick_Released);

	InputComponent->BindAction(TEXT("Focus"), IE_Pressed, this, &AOverviewPawn::OnFocus_Pressed);
	InputComponent->BindAction(TEXT("Focus"), IE_Released, this, &AOverviewPawn::OnFocus_Released);

	InputComponent->BindAction(TEXT("ModFocus"), IE_Pressed, this, &AOverviewPawn::OnModFocus_Pressed);
	InputComponent->BindAction(TEXT("ModFocus"), IE_Released, this, &AOverviewPawn::OnModFocus_Released);

	InputComponent->BindAction(TEXT("Act"), IE_Pressed, this, &AOverviewPawn::OnAct_Pressed);
	InputComponent->BindAction(TEXT("Act"), IE_Released, this, &AOverviewPawn::OnAct_Released);

	InputComponent->BindAction(TEXT("ModAct"), IE_Pressed, this, &AOverviewPawn::OnPick_Pressed);
	InputComponent->BindAction(TEXT("ModAct"), IE_Released, this, &AOverviewPawn::OnPick_Released);
	
	InputComponent->BindAxis(TEXT("PanAxisY"), this, &AOverviewPawn::OnPanAxisY);
	InputComponent->BindAxis(TEXT("PanAxisX"), this, &AOverviewPawn::OnPanAxisX);

	InputComponent->BindAxis(TEXT("HeightAxis"), this, &AOverviewPawn::OnHeightAxis);
	InputComponent->BindAxis(TEXT("ZoomAxis"), this, &AOverviewPawn::OnZoomAxis);

	InputComponent->BindAxis(TEXT("CursorAxisX"), this, &AOverviewPawn::OnCursorAxisX);
	InputComponent->BindAxis(TEXT("CursorAxisY"), this, &AOverviewPawn::OnCursorAxisY);

}

/*****************************************************************************
 * Input Event Handlers                                                      *
 *****************************************************************************/

// Pick

void AOverviewPawn::OnPick_Pressed()
{
	bPickPressed = true;
}

void AOverviewPawn::OnPick_Released()
{
	bPickPressed = false;

	// If drag pick multiselect is being used
	if (SelectionBox.GetVolume() > 0.f)
	{
		UE_LOG(LogTemp, Warning, TEXT("Drag pick attempted without implementation!"));
	}
	else // Regular pick behavior
	{
		UWorld* World = GetWorld();

		if (IsValid(World) && IsValid(PlayerController))
		{
			// Deselect all currently selected actors
			PlayerController->UnselectAllBodies();

			// If this pawn is controlled by a player and is using mouse and keyboard.....
			if (PlayerController && InputContext == EInputContext::MOUSE_AND_KEYBOARD)
			{
				// Trace for objects which block the "Selectable" trace channel
				FHitResult HitData(EForceInit::ForceInit);
				PlayerController->GetHitResultUnderCursor(AB_CC_SELECTABLE, false, HitData);

				// Attempt to select an actor. The function checks for the actor's validity for us.
				ABody* HitActor = Cast<ABody>(HitData.GetActor());
				if (IsValid(HitActor) && HitActor->IsSelectable())
				{
					PlayerController->SelectBody(HitActor);
				}
			}
		}
	}
}

void AOverviewPawn::OnPick_DoubleTapped()
{
}

// ModPick

void AOverviewPawn::OnModPick_Pressed()
{
	bModPickPressed = true;
}

void AOverviewPawn::OnModPick_Released()
{
	bModPickPressed = false;

	// If drag pick multiselect is being used
	if (SelectionBox.GetVolume() > 0.f)
	{
		UE_LOG(LogTemp, Warning, TEXT("Drag mod pick attempted without implementation!"));
	}
	else // Regular pick behavior
	{
		UWorld* World = GetWorld();

		if (IsValid(World) && IsValid(PlayerController))
		{
			// If this pawn is controlled by a player and is using mouse and keyboard.....
			if (PlayerController && InputContext == EInputContext::MOUSE_AND_KEYBOARD)
			{
				// Trace for objects which block the "Selectable" trace channel
				FHitResult HitData(EForceInit::ForceInit);
				PlayerController->GetHitResultUnderCursor(AB_CC_SELECTABLE, false, HitData);

				// Attempt to select an actor. The function checks for the actor's validity for us.
				ABody* HitActor = Cast<ABody>(HitData.GetActor());
				if (IsValid(HitActor) && HitActor->IsSelectable())
				{
					// If the actor was already selected, then deselect it; otherwise, add the actors to selection
					PlayerController->CheckSelection(HitActor) ? PlayerController->UnselectBody(HitActor) :
						PlayerController->SelectBody(HitActor);
				}
			}
		}
	}
}

void AOverviewPawn::OnModPick_DoubleTapped()
{
}

// Focus

void AOverviewPawn::OnFocus_Pressed()
{
	bFocusPressed = true;

	if (IsValid(PlayerController))
	{
		PlayerController->bShowMouseCursor = false;
		PlayerController->GetMousePosition(RotationMousePositionX, RotationMousePositionY);
	}
}

void AOverviewPawn::OnFocus_Released()
{
	bFocusPressed = false;

	if (IsValid(PlayerController))
	{
		PlayerController->bShowMouseCursor = true;
	}
}

void AOverviewPawn::OnFocus_DoubleTapped()
{
}

// ModFocus

void AOverviewPawn::OnModFocus_Pressed()
{
	bModFocusPressed = true;
}

void AOverviewPawn::OnModFocus_Released()
{
	bModFocusPressed = false;
}

void AOverviewPawn::OnModFocus_DoubleTapped()
{
}

// Act

void AOverviewPawn::OnAct_Pressed()
{
	bActPressed = true;
}

void AOverviewPawn::OnAct_Released()
{
	bActPressed = false;
}

void AOverviewPawn::OnAct_DoubleTapped()
{
}

// ModAct

void AOverviewPawn::OnModAct_Pressed()
{
	bModActPressed = true;
}

void AOverviewPawn::OnModAct_Released()
{
	bModActPressed = false;
}

void AOverviewPawn::OnModAct_DoubleTapped()
{
}

void AOverviewPawn::OnPanAxisY(float AxisValue)
{
	if (!FMath::IsNearlyZero(AxisValue))
	{
		AddMovementInput(GetActorForwardVector(), AxisValue);
	}
}

void AOverviewPawn::OnPanAxisX(float AxisValue)
{
	if (!FMath::IsNearlyZero(AxisValue))
	{
		AddMovementInput(GetActorRightVector(), AxisValue);
	}
}

void AOverviewPawn::OnHeightAxis(float AxisValue)
{
	if (!FMath::IsNearlyZero(AxisValue))
	{
		AddMovementInput(GetActorUpVector(), AxisValue);
	}
}

void AOverviewPawn::OnZoomAxis(float AxisValue)
{
	if (!FMath::IsNearlyZero(AxisValue))
	{
		PendingZoom += AxisValue;

		if (InputContext == EInputContext::MOUSE_AND_KEYBOARD)
		{
			check(PlayerController);
			FVector WorldPosition{};
			PlayerController->DeprojectMousePositionToWorld(WorldPosition, ZoomDirection);
		}
		else
		{
			ZoomDirection = CameraComponent->GetForwardVector();
		}
	}
}

void AOverviewPawn::OnCursorAxisX(float AxisValue)
{
	if (InputContext == EInputContext::MOUSE_AND_KEYBOARD && bFocusPressed && FMath::Abs(AxisValue) > 0.0141421356f)
	{
		AddControllerYawInput(AxisValue  * MouseYawFactor);
	}
}

void AOverviewPawn::OnCursorAxisY(float AxisValue)
{
	if (InputContext == EInputContext::MOUSE_AND_KEYBOARD)
	{
		if (bFocusPressed && FMath::Abs(AxisValue) > 0.0141421356f)
		{
			AddControllerPitchInput(AxisValue * MousePitchFactor);
		}
	}
}