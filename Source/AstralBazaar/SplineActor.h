// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "GameFramework/Actor.h"
#include "Components/SplineComponent.h"
#include "SplineActor.generated.h"

UCLASS(Blueprintable)
class ASTRALBAZAAR_API ASplineActor : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASplineActor();

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = SplineActor)
	USplineComponent* SplineComponent;
};
