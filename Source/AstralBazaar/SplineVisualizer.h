// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "GameFramework/Actor.h"
#include "SplineVisualizer.generated.h"

UCLASS(Blueprintable)
class ASTRALBAZAAR_API ASplineVisualizer : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ASplineVisualizer();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	TArray<class USplineMeshComponent*> SplineMeshComponents;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = SplineVisualizer)
	UStaticMesh* SplineMesh;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = SplineVisualizer)
	UMaterial* SplineMaterial;

	int32 NumSplinePoints;
	float TimePerSplinePoint;

public:
	UFUNCTION(BlueprintCallable, Category = SplineVisualizer)
	void SetSplineMeshAndMaterial(UStaticMesh* NewSplineMesh, UMaterial* NewSplineMaterial);

	UFUNCTION(BlueprintCallable, Category = SplineVisualizer)
	virtual void VisualizeSpline(class USplineComponent* SplineToVisualize);
	
};
