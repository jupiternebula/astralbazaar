// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#include "AstralBazaar.h"
#include "JN_CoreBlueprintLibrary.h"

bool UJN_CoreBlueprintLibrary::JN_GetGameBuildVersion(FString& VersionString)
{
	static FString CachedVersionString;
	static bool CachedDevelopmentBuild;

	if (CachedVersionString.IsEmpty())
	{
		GConfig->GetString(
			TEXT("/Script/EngineSettings.GeneralProjectSettings"),
			TEXT("ProjectVersion"),
			CachedVersionString,
			GGameIni);

		CachedDevelopmentBuild = CachedVersionString.Contains("-g", ESearchCase::CaseSensitive);
	}

	VersionString = CachedVersionString;
	return CachedDevelopmentBuild;
}
