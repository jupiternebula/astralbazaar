// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "GameFramework/Pawn.h"
#include "OverviewPawn.generated.h"

UCLASS()
class ASTRALBAZAAR_API AOverviewPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AOverviewPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	USceneComponent* RootSceneComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UFloatingPawnMovement* MovementComponent;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	UCameraComponent* CameraComponent;

	// The owning player component (if controlled by a player)
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly)
	class AAB_PlayerControllerBase* PlayerController;
	/*
	// A set of selected actors
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly)
	TSet<AActor*> SelectedActors;
	*/
	// An FBox representing the selection box (for drag pick multiselection). @TODO: Will change to custom class when implemented
	UPROPERTY(VisibleInstanceOnly, BlueprintReadOnly, Transient)
	FBox SelectionBox;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly)
	EInputContext InputContext; // @TODO: Need to add code to set this to something other than MOUSE_AND_KEYBOARD to be triggered by the player controller.

	// Adjusts the movement component based on either the currently desired zoom value or the current spring arm length.
	FORCEINLINE void AdjustMovementComponent();

private:
	float StoredWorldTimeDilation;

public:
	/*
	// Select actors and add them to the SelectedActors set. Returns the new number of actors selected
	UFUNCTION(BlueprintCallable, Category = "Selectable")
	int32 SelectActors(TArray<AActor*> ActorsToSelect);

	// Select a single actor and add it to the SelectedActors set. Returns the new number of actors selected
	UFUNCTION(BlueprintCallable, Category = "Selectable")
	FORCEINLINE int32 SelectActor(AActor* ActorToSelect);

	// Deselect the passed actor and remove it from the SelectedActors set. Returns false if the actor was not actually selected
	UFUNCTION(BlueprintCallable, Category = "Selectable")
	bool DeselectActor(AActor* ActorToDeselect);

	// Deselect all selected actors and empty the SelectedActors set. Returns the number of actors being deselected
	UFUNCTION(BlueprintCallable, Category = "Selectable")
	int32 DeselectAll();
	*/
	virtual void PossessedBy(class AController* NewController);

	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

/*****************************************************************************
 * Input Event Handlers                                                      *
 *****************************************************************************/
 protected:
	// Pick is currently being held down
	UPROPERTY(BlueprintReadOnly, Transient, Category = InputState)
	uint8 bPickPressed : 1;

	// ModPick is currently being held down
	UPROPERTY(BlueprintReadOnly, Transient, Category = InputState)
	uint8 bModPickPressed : 1;

	// Focus is currently being held down
	UPROPERTY(BlueprintReadOnly, Transient, Category = InputState)
	uint8 bFocusPressed : 1;

	// ModFocus is currently being held down
	UPROPERTY(BlueprintReadOnly, Transient, Category = InputState)
	uint8 bModFocusPressed : 1;

	// Act is currently being held down
	UPROPERTY(BlueprintReadOnly, Transient, Category = InputState)
	uint8 bActPressed : 1;

	// ModAct is currently being held down
	UPROPERTY(BlueprintReadOnly, Transient, Category = InputState)
	uint8 bModActPressed : 1;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float PendingZoom;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float ZoomScale;
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float ZoomInterpSpeed;
	// The minimum Z-height value passed to the movement component's scaling functions
	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	float ZHeightScalingBaseline;

	// Used when zooming to store the direction to move the camera
	FVector ZoomDirection;
	// Location of mouse in screen coordinates when the focus button is pressed in order to restore mouse cursor position when releasing
	float RotationMousePositionX;
	float RotationMousePositionY;

	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PanSpeedScale;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PanAccelerationScale;
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float PanDecelerationScale;

	// A bonus value to modulate mouse yaw. Adjust to user preferences
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MouseYawFactor;
	// A bonus value to modulate mouse yaw. Adjust to user preferences
	UPROPERTY(EditAnywhere, BlueprintReadWrite)
	float MousePitchFactor;

public:
	void OnPick_Pressed();
	void OnPick_Released();
	void OnPick_DoubleTapped();

	void OnModPick_Pressed();
	void OnModPick_Released();
	void OnModPick_DoubleTapped();

	void OnFocus_Pressed();
	void OnFocus_Released();
	void OnFocus_DoubleTapped();

	void OnModFocus_Pressed();
	void OnModFocus_Released();
	void OnModFocus_DoubleTapped();

	void OnAct_Pressed();
	void OnAct_Released();
	void OnAct_DoubleTapped();

	void OnModAct_Pressed();
	void OnModAct_Released();
	void OnModAct_DoubleTapped();
	
	void OnPanAxisY(float AxisValue);
	void OnPanAxisX(float AxisValue);

	void OnHeightAxis(float AxisValue);
	void OnZoomAxis(float AxisValue);

	void OnCursorAxisX(float AxisValue);
	void OnCursorAxisY(float AxisValue);

};
