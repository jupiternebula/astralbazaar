// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "GameFramework/PlayerController.h"
#include "AstralBazaarPlayerControllerBase.generated.h"

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FBodySelectedDelegate, ABody*, SelectedBody, AAB_PlayerControllerBase*, AB_PlayerController);

DECLARE_DYNAMIC_MULTICAST_DELEGATE_TwoParams(FBodyUnselectedDelegate, ABody*, UnselectedBody, AAB_PlayerControllerBase*, AB_PlayerController);
DECLARE_DYNAMIC_MULTICAST_DELEGATE_OneParam(FBodyUnselectedAllDelegate, AAB_PlayerControllerBase*, AB_PlayerController);

UCLASS()
class ASTRALBAZAAR_API AAB_PlayerControllerBase : public APlayerController
{
	GENERATED_BODY()
	
public:
	AAB_PlayerControllerBase();

protected:
	// A set of selected actors
	UPROPERTY(VisibleInstanceOnly, Category = "Selection")
	TSet<class ABody*> SelectedBodies;
	
public:
	// Adds a body to the current set of selected actors
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Selection")
	bool SelectBody(class ABody* BodyToSelect);
	FORCEINLINE bool SelectBody_Implementation(class ABody* BodyToSelect);
/*
	// Adds multiple bodies to the current set of selected actors
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Selection")
	void SelectBodies(TSet<class ABody*> BodiesToSelect);
	FORCEINLINE void SelectBodies_Implementation(TSet<class ABody*> BodiesToSelect);
*/
	// Unregister a body's selection. Expects that the body considers itself to be unselected
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Selection")
	bool UnselectBody(class ABody* BodyToUnselect);
	FORCEINLINE bool UnselectBody_Implementation(class ABody* BodyToUnselect);
/*
	// Unselects multiple bodies at once
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Selection")
	void UnselectBodies(TSet<class ABody*> BodiesToUnselect);
	FORCEINLINE void UnselectBodies_Implementation(TSet<class ABody*> BodiesToUnselect);
*/
	// Unselect all currently selected actors
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, Category = "Selection")
	void UnselectAllBodies();
	FORCEINLINE void UnselectAllBodies_Implementation();

	// Check whether or not a body has been registered as being selected
	UFUNCTION(BlueprintPure, Category = "Selection")
	FORCEINLINE bool CheckSelection(class ABody* BodyToQuery) const;

	// Get a constant reference to the selected bodies
	UFUNCTION(BlueprintPure, Category = "Selection")
	FORCEINLINE TSet<ABody*> GetSelectedBodies() const;

	// Broadcasts when a body has been selected
	UPROPERTY(BlueprintAssignable, Category = "Selection")
	FBodySelectedDelegate BodySelectedDelegate;

	// Broadcasts when a body has been unselected
	UPROPERTY(BlueprintAssignable, Category = "Selection")
	FBodyUnselectedDelegate BodyUnselectedDelegate;
	// Broadcasts when all bodies have been unselected
	UPROPERTY(BlueprintAssignable, Category = "Selection")
	FBodyUnselectedAllDelegate BodyUnselectedAllDelegate;
	
};
