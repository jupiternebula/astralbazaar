// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "Kismet/BlueprintFunctionLibrary.h"
#include "JN_CoreBlueprintLibrary.generated.h"

/**
 * 
 */
UCLASS()
class ASTRALBAZAAR_API UJN_CoreBlueprintLibrary : public UBlueprintFunctionLibrary
{
	GENERATED_BODY()

public:
	// Gets the string representation of the game's build version. If the version string contains a Git reference then it returns true
	UFUNCTION(BlueprintPure, Category = "Jupiter Nebula|Build Configuration")
	static bool JN_GetGameBuildVersion(FString& VersionString);

};
