// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#include "AstralBazaar.h"
#include "AstralBazaarGameStateBase.h"

AAstralBazaarGameStateBase::AAstralBazaarGameStateBase()
{
	PrimaryActorTick.bCanEverTick = true;

	SimulationDateTime = FDateTime{ 2201, 1, 1 };
}

void AAstralBazaarGameStateBase::Tick(float DeltaTime)
{
	SimulationDateTime += FTimespan::FromDays(DeltaTime);
}

FDateTime AAstralBazaarGameStateBase::GetSimulationDateTime() const
{
	return SimulationDateTime;
}