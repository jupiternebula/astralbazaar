// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Body.generated.h"

UENUM()
enum class EBodyHighlightType : uint8
{
	Default  = 0,
	Hovered  = 254,
	Selected = 255
};

UCLASS(Blueprintable, ClassGroup = (Custom))
class ASTRALBAZAAR_API ABody : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABody();

	// When set to true, this body will generate dynamic materials for all attached primitive components with materials and allow shadow volumes to manage them.
	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Body)
	uint8 bReceiveShadows : 1;
	// When set to true, the body simulation will affect the entire transform rather than just its position
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = Body)
	uint8 bSimulateTransform : 1;

protected:
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = Selection)
	uint8 bSelectable : 1;

	uint8 bSelected : 1;
	uint8 bHovered : 1;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Body)
	FText BodyName;

	// Used to decide which primitives are highlighted when hovering and selecting
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = Selection)
	TArray<UPrimitiveComponent*> SelectionPrimitives;

	UPROPERTY(BlueprintReadOnly, EditDefaultsOnly, Category = Selection)
	uint8 StencilValueHovered;
	UPROPERTY(BlueprintReadWrite, EditDefaultsOnly, Category = Selection)
	uint8 StencilValueSelected;

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	virtual void AdvanceSimulation(float DeltaTime);

private:
	// Cached cast of the attach parent actor to ABody
	ABody* AttachParentBody;

	// Highlights the body using a custom stencil value
	void HighlightBody(EBodyHighlightType HighlightType);

public:	
	virtual bool Modify(bool bAlwaysMarkDirty) override;
	virtual void Tick(float DeltaTime) override;
	virtual void PostInitializeComponents() override;

	UFUNCTION(BlueprintCallable, Category = Body)
	ABody* GetAttachParentBody();

	UFUNCTION(BlueprintPure, Category = Body)
	FORCEINLINE FText GetBodyName() const;

	// Retrieves the squared distance of the body for sorting purposes. This can be overridden for specific sorting criteria for other classes
	UFUNCTION(BlueprintPure, Category = Body)
	virtual float GetSortingDistanceSquared() const;

	FORCEINLINE bool operator<(const ABody& OtherBody) const;

	UFUNCTION(BlueprintPure, Category = Body)
	void GetAttachedBodies(TArray<ABody*>& AttachedBodies, bool bSortArray = false) const;

	UFUNCTION(BlueprintCallable, Category = Body)
	virtual FVector PredictRelativeLocation(float DeltaTime);
	UFUNCTION(BlueprintCallable, Category = Body)
	virtual FTransform PredictRelativeTransform(float DeltaTime);
	UFUNCTION(BlueprintCallable, Category = Body)
	FVector PredictLocation(float DeltaTime);
	UFUNCTION(BlueprintCallable, Category = Body)
	FTransform PredictTransform(float DeltaTime);
	
	// Returns whether or not this body can be selected.
	UFUNCTION(BlueprintPure, Category = Selection)
	FORCEINLINE bool IsSelectable() const;

	UFUNCTION(BlueprintPure, BlueprintCosmetic, Category = Selection)
	FORCEINLINE bool IsHovered() const;

	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, BlueprintCosmetic, Category = "Selection", meta = (HidePin = "HoveredActor"))
	void OnBeginHover(AActor* HoveredActor);
	virtual void OnBeginHover_Implementation(AActor* HoveredActor);
	UFUNCTION(BlueprintCallable, BlueprintNativeEvent, BlueprintCosmetic, Category = "Selection", meta = (HidePin = "UnhoveredActor"))
	void OnEndHover(AActor* UnhoveredActor);
	virtual void OnEndHover_Implementation(AActor* UnhoveredActor);

	UFUNCTION(BlueprintNativeEvent, Category = "Selection")
	void OnSelect(class AAB_PlayerControllerBase* AB_PlayerController);
	virtual void OnSelect_Implementation(class AAB_PlayerControllerBase* AB_PlayerController);

	UFUNCTION(BlueprintNativeEvent, Category = "Selection")
	void OnUnselect(class AAB_PlayerControllerBase* AB_PlayerController);
	virtual void OnUnselect_Implementation(class AAB_PlayerControllerBase* AB_PlayerController);

	virtual void PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent) override;
};
