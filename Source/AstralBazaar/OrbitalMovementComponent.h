// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.
/*
#pragma once

#include "Components/ActorComponent.h"
#include "OrbitalMovementComponent.generated.h"

USTRUCT(BlueprintType)
struct FOrbitalParameters
{
	GENERATED_BODY()

	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters)
	float Apoapsis;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters)
	float Periapsis;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters, meta = (ClampMin = -180.0, ClampMax = 180.0))
	float Inclination;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters, meta = (ClampMin = 0.0001))
	float CycleDuration;
	UPROPERTY(BlueprintReadWrite, EditAnywhere, Category = OrbitalParameters)
	float Phase;

	// Ensure that the provided parameters are within the correct ranges.
	void Validate()
	{
		Apoapsis      = FMath::Max(Apoapsis, KINDA_SMALL_NUMBER);
		Periapsis     = FMath::Clamp<float>(Periapsis, Apoapsis / 3.f, Apoapsis);
		Inclination   = FMath::UnwindDegrees(Inclination);
		CycleDuration = FMath::Max(CycleDuration, KINDA_SMALL_NUMBER);
		Phase         = FMath::Fmod(Phase, CycleDuration);
	}

	FOrbitalParameters()
	{
		Apoapsis      = KINDA_SMALL_NUMBER;
		Periapsis     = KINDA_SMALL_NUMBER;
		Inclination   = 0.f;
		CycleDuration = 10.f;
		Phase         = 0.f;
	}
};

UCLASS( ClassGroup=(Custom), meta=(BlueprintSpawnableComponent) )
class ASTRALBAZAAR_API UOrbitalMovementComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UOrbitalMovementComponent();

#if WITH_EDITOR
	virtual void PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent) override;
#endif

	virtual void OnRegister() override;

	UPROPERTY(BlueprintReadOnly, EditAnywhere, Category = OrbitalParameters, meta = (ShowOnlyInnerProperties))
	FOrbitalParameters OrbitalParameters;

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere)
	class USplineComponent* OrbitSpline;

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UFUNCTION(BlueprintCallable, Category = OrbitalMovementComponent)
	void SetOrbitalParameters(const FOrbitalParameters& NewOrbitalParameters);
	
};
*/