// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "Selectable.generated.h"

// This class does not need to be modified.
UINTERFACE(MinimalAPI)
class USelectable : public UInterface
{
	GENERATED_UINTERFACE_BODY()
};

/**
 * 
 */
class ASTRALBAZAAR_API ISelectable
{
	GENERATED_IINTERFACE_BODY()

	// Add interface functions to this class. This is the class that will be inherited to implement this interface.
public:
	
	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Selectable")
	void OnSelect(bool bMultipleSelection);

	UFUNCTION(BlueprintCallable, BlueprintImplementableEvent, Category = "Selectable")
	void OnDeselect();

};
