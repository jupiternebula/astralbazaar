// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.
/*
#include "AstralBazaar.h"
#include "Components/SplineComponent.h"
#include "OrbitalMovementComponent.h"


// Sets default values for this component's properties
UOrbitalMovementComponent::UOrbitalMovementComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;
}

#if WITH_EDITOR
void UOrbitalMovementComponent::PostEditChangeChainProperty(FPropertyChangedChainEvent& PropertyChangedEvent)
{
	if (PropertyChangedEvent.PropertyChain.GetHead()->GetValue()->GetFName() == GET_MEMBER_NAME_CHECKED(UOrbitalMovementComponent, OrbitalParameters))
	{
		OrbitalParameters.Validate();
	}

	Super::PostEditChangeChainProperty(PropertyChangedEvent);
}
#endif

void UOrbitalMovementComponent::OnRegister()
{
	Super::OnRegister();

	

}

// Called when the game starts
void UOrbitalMovementComponent::BeginPlay()
{
	Super::BeginPlay();

	OrbitalParameters.Validate();
	
}


// Called every frame
void UOrbitalMovementComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UOrbitalMovementComponent::SetOrbitalParameters(const FOrbitalParameters& NewOrbitalParameters)
{
	OrbitalParameters = NewOrbitalParameters;
	OrbitalParameters.Validate();
}
*/