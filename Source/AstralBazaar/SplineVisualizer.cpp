// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#include "AstralBazaar.h"
#include "Components/SplineComponent.h"
#include "Components/SplineMeshComponent.h"
#include "SplineVisualizer.h"


// Sets default values
ASplineVisualizer::ASplineVisualizer()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = false;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootSceneComponent"));
}

// Called when the game starts or when spawned
void ASplineVisualizer::BeginPlay()
{
	Super::BeginPlay();
	
}

void ASplineVisualizer::SetSplineMeshAndMaterial(UStaticMesh* NewSplineMesh, UMaterial* NewSplineMaterial)
{
	if (IsValid(NewSplineMesh))
		SplineMesh = NewSplineMesh;
	if (IsValid(NewSplineMaterial))
		SplineMaterial = NewSplineMaterial;
	/*
	USplineComponent* VisualizedSplineComponent = Cast<USplineComponent>(RootComponent->GetAttachParent());
	if (SplineMeshComponents.Num() > 0 && IsValid(VisualizedSplineComponent))
		VisualizeSpline(VisualizedSplineComponent);
	*/
}

void ASplineVisualizer::VisualizeSpline(USplineComponent* SplineToVisualize)
{
	// Make sure we are being passed a valid USplineComponent
	if (!IsValid(SplineToVisualize))
	{
		UE_LOG(LogTemp, Warning, TEXT("Tried to visualize an invalid spline component. Ignoring visualization request."));
		return;
	}
	if (SplineToVisualize->GetNumberOfSplinePoints() < 2)
	{
		UE_LOG(LogTemp, Warning, TEXT("Tried to visualize a spline component with too few spline points. Returning."));
		return;
	}
	if (!IsValid(SplineMesh) || !IsValid(SplineMaterial))
	{
		UE_LOG(LogTemp, Error, TEXT("Attempted to build spline meshes without having a static mesh and material defined! Returning."));
		return;
	}

	if (SplineToVisualize->Mobility == EComponentMobility::Static)
		RootComponent->SetMobility(EComponentMobility::Static);
	RootComponent->AttachToComponent(SplineToVisualize, FAttachmentTransformRules::SnapToTargetIncludingScale);
	
	// If the array already contains spline mesh components, then destroy them. Not ideal, but good enough for our purposes
	if (SplineMeshComponents.Num() > 0)
	{
		for (auto& SMC : SplineMeshComponents)
			SMC->DestroyComponent();
	}

	int32 NumNeededSplineMeshes = SplineToVisualize->IsClosedLoop() ? SplineToVisualize->GetNumberOfSplinePoints() :
		SplineToVisualize->GetNumberOfSplinePoints() - 1;
	SplineMeshComponents.Empty(NumNeededSplineMeshes);

	// Generate the needed spline mesh components. If the loop goes to the number of spline points,
	// then this is a closed loop and the final mesh will end at spline point 0.
	for (int32 i = 0, j = 1; i < NumNeededSplineMeshes; i++, j = (i >= SplineToVisualize->GetNumberOfSplinePoints() ? 0 : i + 1))
	{
		USplineMeshComponent* NewSplineMeshComponent = NewObject<USplineMeshComponent>(this);
		NewSplineMeshComponent->RegisterComponent();
		NewSplineMeshComponent->SetMobility(RootComponent->Mobility);
		NewSplineMeshComponent->AttachToComponent(RootComponent, FAttachmentTransformRules::SnapToTargetIncludingScale);
		
		NewSplineMeshComponent->SetStaticMesh(SplineMesh);
		NewSplineMeshComponent->SetMaterial(0, SplineMaterial);

		NewSplineMeshComponent->SetStartAndEnd(
			SplineToVisualize->GetLocationAtSplinePoint(i, ESplineCoordinateSpace::World),
			SplineToVisualize->GetLeaveTangentAtSplinePoint(i, ESplineCoordinateSpace::World),
			SplineToVisualize->GetLocationAtSplinePoint(j, ESplineCoordinateSpace::World),
			SplineToVisualize->GetArriveTangentAtSplinePoint(j, ESplineCoordinateSpace::World));

		SplineMeshComponents.Push(NewSplineMeshComponent);
	}
}
