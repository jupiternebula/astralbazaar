// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "GameFramework/Pawn.h"
#include "GameViewPawn.generated.h"

UCLASS()
class ASTRALBAZAAR_API AGameViewPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	AGameViewPawn();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* PlayerInputComponent) override;

	
	
};
