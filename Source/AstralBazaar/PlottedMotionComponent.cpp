// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#include "AstralBazaar.h"
#include "PlottedMotionIndicator.h"
#include "PlottedMotionComponent.h"


// Sets default values for this component's properties
UDEPRECATED_PlottedMotionComponent::UDEPRECATED_PlottedMotionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	LoopType = EPlottedMotionLoopType::Clamped;
	bShowDebugHelpers = false;
	bAdvanceActorAsOffset = true;
	StartingPhase = 0.f;
	CycleLength = 1.f;
	CycleDuration = 1.f;
	CurrentPhase = StartingPhase;
	SecondsActive = 0.f;
	bActive = false; // @TODO: Should probably set this to false and force classes to call Start()
	
	static ConstructorHelpers::FObjectFinder<UBlueprint> PlottedMotionIndicatorBP(TEXT("/Game/Core/PlottedMotionIndicators/BP_PlottedMotionIndicator"));
	IndicatorClass = PlottedMotionIndicatorBP.Object->GeneratedClass;
}

void UDEPRECATED_PlottedMotionComponent::OnRegister()
{
	Super::OnRegister();

	CalculatePath();

	// @TODO: Below only needs to be called while in the editor
	FVector VectorOffset(0.f, 0.f, 0.f);
	if (GetOwner()->GetAttachParentActor())
	{
		VectorOffset = GetOwner()->GetAttachParentActor()->GetActorLocation();
	}
	GetOwner()->SetActorLocation(VectorOffset + PredictOffsetVector(0.f));
}

float UDEPRECATED_PlottedMotionComponent::DeltaTimeToPhase(float DeltaTime) const
{
	float NewPhase = CurrentPhase + DeltaTime * CycleLength / CycleDuration;

	switch (LoopType)
	{
	case EPlottedMotionLoopType::OpenEnded:
		return NewPhase;
	case EPlottedMotionLoopType::Pulsating:
		// @TODO: Add check for out of range starting phase
		// @TODO: Add support for negative delta time values (Perhaps just stick an absolute value in the if statement?)
		if (FMath::Fmod(SecondsActive + DeltaTime, 2.f * CycleDuration) >= CycleDuration)
			return CycleLength;
	case EPlottedMotionLoopType::Continuous:
		return FMath::Fmod(NewPhase, CycleLength);
	case EPlottedMotionLoopType::Clamped:
	default:
		return FMath::Clamp(NewPhase, -CycleLength, CycleLength);
	}
}

FVector UDEPRECATED_PlottedMotionComponent::PredictOffsetVector(float DeltaTime, float& NewPhase) const
{
	NewPhase = DeltaTimeToPhase(DeltaTime);
	return PredictOffsetVectorAtPhase(NewPhase);
}

FVector UDEPRECATED_PlottedMotionComponent::PredictOffsetVector(float DeltaTime) const
{
	return PredictOffsetVectorAtPhase(DeltaTimeToPhase(DeltaTime));
}

float UDEPRECATED_PlottedMotionComponent::GetCurrentPhase() const
{
	return CurrentPhase;
}

/* @TODO: Implement later
void UDEPRECATED_PlottedMotionComponent::SetCurrentPhase(float NewPhase)
{
}
*/

float UDEPRECATED_PlottedMotionComponent::AdvanceActor(float DeltaTime)
{
	GetOwner()->SetActorRelativeLocation(PredictOffsetVector(DeltaTime, CurrentPhase));

	return CurrentPhase;
}

void UDEPRECATED_PlottedMotionComponent::Start()
{
	CurrentPhase = StartingPhase;
	SecondsActive = 0.f;
	bActive = true;
}

void UDEPRECATED_PlottedMotionComponent::Resume()
{
	bActive = true;
}

void UDEPRECATED_PlottedMotionComponent::Stop()
{
	bActive = false;
}

ADEPRECATED_PlottedMotionIndicator* UDEPRECATED_PlottedMotionComponent::SpawnIndicator(float AnimationTime, bool ScaleToCycleDuration)
{
	UWorld* World = GetWorld();
	if (IsValid(World))
	{
		AActor* OwnerAttachParent = GetOwner()->GetAttachParentActor();

		ADEPRECATED_PlottedMotionIndicator* NewPlottedMotionIndicator = World->SpawnActor<ADEPRECATED_PlottedMotionIndicator>(IndicatorClass.Get(),
			GetOwner()->GetActorLocation(), FRotator::ZeroRotator);
	
		// @NOTE: This IsValid check is probably redundant
		if (IsValid(NewPlottedMotionIndicator))
		{
			if (IsValid(OwnerAttachParent))
			{
				NewPlottedMotionIndicator->AttachToActor(OwnerAttachParent, FAttachmentTransformRules(EAttachmentRule::SnapToTarget, false));
			}

			/* FUNCTION WILL NOT DEPRECATE! WHAT IS WRONG WITH UNREAL!!!!!!!!!!!!!!!!!!!!!!!!!
			NewPlottedMotionIndicator->VisualizePlottedMotionComponent_DEPRECATED(this, AnimationTime, ScaleToCycleDuration);
			return NewPlottedMotionIndicator;
			*/
		}
	}

	return nullptr;
}

void UDEPRECATED_PlottedMotionComponent::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	CalculatePath();
}

// Called every frame
void UDEPRECATED_PlottedMotionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	if (bActive)
	{
		AdvanceActor(DeltaTime);
		SecondsActive += DeltaTime;
	}
}

