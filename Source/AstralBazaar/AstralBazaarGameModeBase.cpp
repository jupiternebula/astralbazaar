// Fill out your copyright notice in the Description page of Project Settings.

#include "AstralBazaar.h"
#include "OverviewPawn.h"
#include "AstralBazaarGameStateBase.h"
#include "AstralBazaarGameModeBase.h"

AAstralBazaarGameModeBase::AAstralBazaarGameModeBase()
{
	DefaultPawnClass = AOverviewPawn::StaticClass();
	GameStateClass = AAstralBazaarGameStateBase::StaticClass();
	
	static ConstructorHelpers::FObjectFinder<UBlueprint> DefaultPlayerControllerBP(TEXT("/Game/Core/PlayerControllers/BP_DefaultPlayerController"));
	PlayerControllerClass = DefaultPlayerControllerBP.Object->GeneratedClass;
}

float AAstralBazaarGameModeBase::GetMaxGlobalTimeDilation() const
{
	AWorldSettings* WorldSettings = GetWorldSettings();
	return IsValid(WorldSettings) ? WorldSettings->MaxGlobalTimeDilation : 20.f; // 20.f is the default value for this in Unreal
}

float AAstralBazaarGameModeBase::GetMinGlobalTimeDilation() const
{
	AWorldSettings* WorldSettings = GetWorldSettings();
	return IsValid(WorldSettings) ? WorldSettings->MinGlobalTimeDilation : 0.0001f; // 0.0001f is the default value for this in Unreal
}
