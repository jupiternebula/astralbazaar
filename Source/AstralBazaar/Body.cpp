// Fill out your copyright notice in the Description page of Project Settings.

#include "AstralBazaar.h"
#include "Components/SplineComponent.h"
#include "AstralBazaarPlayerControllerBase.h"
#include "Body.h"


// Sets default values
ABody::ABody()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;

	RootComponent = CreateDefaultSubobject<USceneComponent>(TEXT("RootComponent"));

	bReceiveShadows = true;
	bSimulateTransform = true;
	
	bSelectable = false;
	bSelected = false;
	bHovered = false;

	StencilValueSelected = 255;
	StencilValueHovered = 254;
}

void ABody::HighlightBody(EBodyHighlightType HighlightType)
{
	if (HighlightType != EBodyHighlightType::Default)
	{
		for (auto& Prim : SelectionPrimitives)
		{
			Prim->SetRenderCustomDepth(true);
			Prim->SetCustomDepthStencilValue(static_cast<int32>(HighlightType));
		}
	}
	else
	{
		if (bHovered)
		{
			HighlightBody(EBodyHighlightType::Hovered);
		}
		else if (bSelected)
		{
			HighlightBody(EBodyHighlightType::Selected);
		}
		else
		{
			for (auto& Prim : SelectionPrimitives)
			{
				Prim->SetRenderCustomDepth(false);
			}
		}
	}
}

// HACK: Probably redundant to call reconstruct the orbital spline in OnConstruction and PostInitializeComponents
bool ABody::Modify(bool bAlwaysMarkDirty)
{
	AdvanceSimulation(0.f);

	return Super::Modify(bAlwaysMarkDirty);
}

// Called when the game starts or when spawned
void ABody::BeginPlay()
{
	Super::BeginPlay();

	// HACK: Need to devise a way to get unique body names that works in the final build
	if (BodyName.IsEmptyOrWhitespace())
	{
		BodyName = FText::FromString(this->GetName());
	}

	OnBeginCursorOver.AddDynamic(this, &ABody::OnBeginHover);
	OnEndCursorOver.AddDynamic(this, &ABody::OnEndHover);
	
	AdvanceSimulation(0.f);
}

void ABody::AdvanceSimulation(float DeltaTime)
{
	if (IsValid(RootComponent) && RootComponent->Mobility == EComponentMobility::Movable)
	{
		if (bSimulateTransform)
			SetActorRelativeTransform(PredictRelativeTransform(DeltaTime));
		else
			SetActorRelativeLocation(PredictRelativeLocation(DeltaTime));
	}
}

// Called every frame
void ABody::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);
	
	AdvanceSimulation(DeltaTime);
}

void ABody::PostInitializeComponents()
{
	Super::PostInitializeComponents();

	if (bReceiveShadows)
	{
		for (UActorComponent* CurActorComp : GetComponentsByClass(UPrimitiveComponent::StaticClass()))
		{
			UPrimitiveComponent* CurPrimComp = Cast<UPrimitiveComponent>(CurActorComp);

			for (int32 i = 0; i < CurPrimComp->GetNumMaterials(); i++)
			{
				if (CurPrimComp != nullptr)
				{
					CurPrimComp->CreateAndSetMaterialInstanceDynamic(i);
				}
			}
		}
	}

	AdvanceSimulation(0.f);
}

ABody* ABody::GetAttachParentBody()
{
	if (AttachParentBody != GetAttachParentActor())
		AttachParentBody = Cast<ABody>(GetAttachParentActor());
	return AttachParentBody;
}

FText ABody::GetBodyName() const
{
	return BodyName;
}

float ABody::GetSortingDistanceSquared() const
{
	return GetActorLocation().SizeSquared();
}

bool ABody::operator<(const ABody& OtherBody) const
{
	return GetSortingDistanceSquared() < OtherBody.GetSortingDistanceSquared();
}

void ABody::GetAttachedBodies(TArray<ABody*>& AttachedBodies, bool bSortArray) const
{
	TArray<AActor*> ActorArray;
	GetAttachedActors(ActorArray);

	AttachedBodies.Reset(ActorArray.Num());

	for (auto& Actor : ActorArray)
	{
		if (Actor->IsA<ABody>())
		{
			AttachedBodies.Add(static_cast<ABody*>(Actor));
		}
	}

	if (bSortArray)
	{
		AttachedBodies.Sort();
	}
}

FVector ABody::PredictRelativeLocation(float DeltaTime)
{
	return FVector{};
}

FTransform ABody::PredictRelativeTransform(float DeltaTime)
{
	return FTransform{};
}


FVector ABody::PredictLocation(float DeltaTime)
{
	if (IsValid(GetAttachParentBody()))
		return GetAttachParentBody()->PredictLocation(DeltaTime) + PredictRelativeLocation(DeltaTime);
	else
		return GetActorLocation();
}

FTransform ABody::PredictTransform(float DeltaTime)
{
	if (IsValid(GetAttachParentBody()))
		return GetAttachParentBody()->PredictTransform(DeltaTime) + PredictRelativeTransform(DeltaTime);
	else
		return GetActorTransform();
}

/*
bool ABody::Select()
{
	if (bSelectable && !bSelected)
	{
		bSelected = true;
		OnSelection();
		OnSelectionDelegate.Broadcast(this);
	}

	return bSelected;
}

bool ABody::Deselect()
{
	if (bSelected)
	{
		OnDeselection();
		bSelected = false;
		OnDeselectionDelegate.Broadcast(this);
	}

	return bSelected;
}

bool ABody::IsSelected() const
{
	return bSelected;
}

bool ABody::IsHovered() const
{
	return bHovered;
}
*/

bool ABody::IsSelectable() const
{
	return bSelectable;
}

bool ABody::IsHovered() const
{
	return bHovered;
}

/*
void ABody::OnSelection_Implementation()
{
	for (auto& Prim : SelectionPrimitives)
	{
		Prim->SetRenderCustomDepth(true);
		Prim->SetCustomDepthStencilValue(StencilValueSelected);
	}

	
}

void ABody::OnDeselection_Implementation()
{
	for (auto& Prim : SelectionPrimitives)
	{
		if (IsValid(Prim))
		{
			if (bHovered)
			{
				Prim->SetCustomDepthStencilValue(StencilValueHovered);
			}
			else
			{
				Prim->SetRenderCustomDepth(false);
			}
		}
	}
}

void ABody::OnBeginHover_Implementation()
{
	for (auto& Prim : SelectionPrimitives)
	{
		if (IsValid(Prim))
		{
			Prim->SetRenderCustomDepth(true);
			Prim->SetCustomDepthStencilValue(StencilValueHovered);
		}
	}

	bHovered = true;
	OnHoverDelegate.Broadcast(this);
}

void ABody::OnEndHover_Implementation()
{
	for (auto& Prim : SelectionPrimitives)
	{
		if (IsValid(Prim))
		{
			if (bSelected)
			{
				Prim->SetCustomDepthStencilValue(StencilValueSelected);
			}
			else
			{
				Prim->SetRenderCustomDepth(false);
			}
		}
	}

	bHovered = false;
	OnUnhoverDelegate.Broadcast(this);
}
*/

void ABody::OnBeginHover_Implementation(AActor* HoveredActor)
{
	if (bSelectable)
	{
		if (!bHovered)
		{
			HighlightBody(EBodyHighlightType::Hovered);
		}

		bHovered = true;
	}
}

void ABody::OnEndHover_Implementation(AActor* HoveredActor)
{
	if (bHovered)
	{
		bHovered = false;
		HighlightBody(EBodyHighlightType::Default);
	}
}

void ABody::OnSelect_Implementation(class AAB_PlayerControllerBase* AB_PlayerController)
{
	if (bSelectable)
	{
		if (!bSelected && !bHovered)
		{
			HighlightBody(EBodyHighlightType::Selected);
		}

		bSelected = true;
	}
}

void ABody::OnUnselect_Implementation(class AAB_PlayerControllerBase* AB_PlayerController)
{
	if (bSelected)
	{
		bSelected = false;
		HighlightBody(EBodyHighlightType::Default);
	}
}

void ABody::PostEditChangeProperty(FPropertyChangedEvent& PropertyChangedEvent)
{
	Super::PostEditChangeProperty(PropertyChangedEvent);

	AdvanceSimulation(0.f);
}