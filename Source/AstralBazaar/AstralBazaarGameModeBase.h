// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameModeBase.h"
#include "AstralBazaarGameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class ASTRALBAZAAR_API AAstralBazaarGameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
public:	
	AAstralBazaarGameModeBase();

	UFUNCTION(BlueprintPure, Category = "World Settings")
	FORCEINLINE float GetMaxGlobalTimeDilation() const;

	UFUNCTION(BlueprintPure, Category = "World Settings")
	FORCEINLINE float GetMinGlobalTimeDilation() const;
};
