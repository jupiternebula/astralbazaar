// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "GameFramework/Actor.h"
#include "PlottedMotionIndicator.generated.h"

UCLASS(Deprecated, Blueprintable, NotPlaceable)
class ASTRALBAZAAR_API ADEPRECATED_PlottedMotionIndicator : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ADEPRECATED_PlottedMotionIndicator();

protected:
	virtual void BeginPlay() override;

	UPROPERTY()
	const class UDEPRECATED_PlottedMotionComponent* VisualizedPlottedMotionComponent_DEPRECATED;
	UPROPERTY()
	class UDEPRECATED_PlottedMotionComponent* ClonedPlottedMotionComponent_DEPRECATED;
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = PlottedMotionIndicator)
	UParticleSystemComponent* ParticleSystem;
public:
	/* WILL NOT DEPRECATE WHAT IS WRONG WITH UNREAL!!!!!!!!!!!!!!
	UFUNCTION(BlueprintCallable, Category = PlottedMotionIndicator, meta=(DeprecatedFunction))
	void VisualizePlottedMotionComponent_DEPRECATED(UDEPRECATED_PlottedMotionComponent* ComponentToVisualize_DEPRECATED, float AnimationTime, bool ScaleToCycleDuration);
	*/

	// Called every frame
	virtual void Tick(float DeltaTime) override;
};
