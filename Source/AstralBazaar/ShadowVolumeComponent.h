// Copyright (C) 2017 Joshua William Taylor. All rights reserved.

#pragma once

#include "Components/SphereComponent.h"
#include "ShadowVolumeComponent.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class ASTRALBAZAAR_API UShadowVolumeComponent : public USphereComponent
{
	GENERATED_BODY()

protected:
	virtual void BeginPlay() override;
	virtual void OnRegister() override;

public:
	UShadowVolumeComponent();
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = Debug)
	bool bShowDebugHelpers;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ShadowVolume)
	FName ShadowCasterName;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = ShadowVolume)
	float ShadowCasterRadius;

	UFUNCTION()
	void OnOverlapBegin(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp,
		int32 OtherBodyIndex, bool bFromSweep, const FHitResult& SweepResult);
	UFUNCTION()
	void OnOverlapEnd(UPrimitiveComponent* OverlappedComp, AActor* OtherActor, UPrimitiveComponent* OtherComp, int32 OtherBodyIndex);

	UFUNCTION(BlueprintCallable, Category = ShadowVolume)
	bool ManageMIDs(AActor* ActorIn);

	UFUNCTION(BlueprintCallable, Category = ShadowVolume)
	bool UnmanageMIDs(AActor* ActorIn);

protected:
	TMultiMap<AActor*, UMaterialInstanceDynamic*> ManagedMIDs;

	FLinearColor GetShadowCasterLinearColor();
};
