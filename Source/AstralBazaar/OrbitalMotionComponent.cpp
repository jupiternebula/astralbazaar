// Copyright (C) 2017 Joshua William Taylor. All rights reserved.

#include "AstralBazaar.h"
#include "DrawDebugHelpers.h"
#include "OrbitalMotionComponent.h"


// Sets default values for this component's properties
UDEPRECATED_OrbitalMotionComponent::UDEPRECATED_OrbitalMotionComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	CycleDuration = 87.97f;
	Periapsis = 1150.0253f;
	Apoapsis = 1745.436f;
	Inclination = 7.f;
	StartingPhase = 0.f;
	RotationalPeriod = 58.125f;
	OrbitalBodyRotation = EOrbitalRotationParameter::Disabled;
	bShowDebugHelpers = false;
	LoopType = EPlottedMotionLoopType::Continuous;
	bAdvanceActorAsOffset = true;
	CycleLength = 2 * PI;

	CurrentPhase = 0.f;
	TrueRadius = 0.f;
	TrueCenterOffset = 0.f;
	EccentricityFactor = 0.f;

	static ConstructorHelpers::FObjectFinder<UBlueprint> OrbitalMotionIndicatorBP(TEXT("/Game/Core/PlottedMotionIndicators/BP_OrbitalMotionIndicator"));
	IndicatorClass = OrbitalMotionIndicatorBP.Object->GeneratedClass;
}

void UDEPRECATED_OrbitalMotionComponent::PostEditChangeProperty(FPropertyChangedEvent& PropChangedEvent)
{
	Super::PostEditChangeProperty(PropChangedEvent);

	FName PropertyName = (PropChangedEvent.Property != nullptr) ? PropChangedEvent.Property->GetFName() : NAME_None;

	if ((PropertyName == GET_MEMBER_NAME_CHECKED(UDEPRECATED_OrbitalMotionComponent, Periapsis)))
	{
		Periapsis = FMath::Clamp<float>(Periapsis, 0.f, Apoapsis);
		Apoapsis = FMath::Clamp<float>(Apoapsis, Periapsis, 3.f * Periapsis);
	}
	if ((PropertyName == GET_MEMBER_NAME_CHECKED(UDEPRECATED_OrbitalMotionComponent, Apoapsis)))
	{
		Apoapsis = FMath::Clamp<float>(Apoapsis, Periapsis, INFINITY);
		Periapsis = FMath::Clamp<float>(Periapsis, FMath::Max<float>(0.f, Apoapsis / 3.f), Apoapsis);
	}
}

FVector UDEPRECATED_OrbitalMotionComponent::PredictOffsetVectorAtPhase(float PredictedPhase) const
{
	FVector PredictionVector(0.f, 0.f, 0.f);

	FMath::SinCos(&(PredictionVector.Y), &(PredictionVector.X), PredictedPhase);
	PredictionVector.X = PredictionVector.X * TrueRadius + TrueCenterOffset;
	PredictionVector.Y *= EccentricityFactor;

	return PredictionVector.RotateAngleAxis(Inclination, FVector(0.f, 1.f, 0.f));
}

void UDEPRECATED_OrbitalMotionComponent::CalculatePath()
{
	TrueRadius = (Apoapsis + Periapsis) / 2.f;
	TrueCenterOffset = (Apoapsis - Periapsis) / 2.f;
	EccentricityFactor = FMath::Sqrt(FMath::Pow(Periapsis * 2.f, 2.f) - FMath::Pow(TrueRadius * 2.f - Periapsis * 2.f, 2.f)) / 2.f;
	CurrentPhase = StartingPhase;
}

void UDEPRECATED_OrbitalMotionComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	switch (OrbitalBodyRotation)
	{
	case EOrbitalRotationParameter::ForwardToTangent:
	case EOrbitalRotationParameter::ForwardToCenter:
		UE_LOG(LogTemp, Warning, TEXT("OrbitalMotionComponent on %s specified an orbital body rotation parameter. This has not been implemented yet. Disabling..."),
			*GetOwner()->GetName());
		OrbitalBodyRotation = EOrbitalRotationParameter::Disabled;
		break;
	case EOrbitalRotationParameter::Disabled:
	default:
		break;
	}
}

void UDEPRECATED_OrbitalMotionComponent::BeginPlay()
{
	Super::BeginPlay();
}