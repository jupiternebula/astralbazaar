// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "Engine.h"

UENUM(BlueprintType)
enum class EInputContext : uint8
{
	UNKNOWN,
	MOUSE_AND_KEYBOARD,
	TOUCH,
	GAMEPAD
};

// Custom collision channels
#define AB_CC_SELECTABLE ECollisionChannel::ECC_GameTraceChannel1

// Macros for bitmasks
#define AB_CHECK_FLAG(ENUM_UINT_TYPE, VAR, FLAG) ((VAR & (1 << static_cast<ENUM_UINT_TYPE>(FLAG))) > 0)
#define AB_SET_FLAG(ENUM_UINT_TYPE, VAR, FLAG) (VAR |= (1 << static_cast<ENUM_UINT_TYPE>(FLAG)))
#define AB_SET_FLAG(ENUM_UINT_TYPE, VAR, FLAG) (VAR &= ~(1 << static_cast<ENUM_UINT_TYPE>(FLAG)))
