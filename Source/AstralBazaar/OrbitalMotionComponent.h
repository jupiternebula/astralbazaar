// Copyright (C) 2017 Joshua William Taylor. All rights reserved.

#pragma once

#include "PlottedMotionComponent.h"
#include "OrbitalMotionComponent.generated.h"

UENUM(BlueprintType)
enum class EOrbitalRotationParameter : uint8
{
	Disabled,
	ForwardToTangent,
	ForwardToCenter
};

UCLASS(Deprecated, ClassGroup=(PlottedMotion), meta=(BlueprintSpawnableComponent) )
class ASTRALBAZAAR_API UDEPRECATED_OrbitalMotionComponent : public UDEPRECATED_PlottedMotionComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDEPRECATED_OrbitalMotionComponent();

	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;

protected:
	virtual void BeginPlay() override;

public:
	virtual void PostEditChangeProperty(FPropertyChangedEvent&) override;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OrbitalMotion)
	float Periapsis;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OrbitalMotion)
	float Apoapsis;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OrbitalMotion)
	float Inclination;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OrbitalMotion)
	float RotationalPeriod;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = OrbitalMotion)
	EOrbitalRotationParameter OrbitalBodyRotation;

	virtual FVector PredictOffsetVectorAtPhase(float PredictedPhase) const override;



protected:
	virtual void CalculatePath() override;

private:
	float TrueRadius;
	float TrueCenterOffset;
	float EccentricityFactor;
};
