// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.
/*
#pragma once

#include "GameFramework/Actor.h"
#include "Trajectory.generated.h"

UCLASS()
class ASTRALBAZAAR_API ATrajectory : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ATrajectory();

protected:
	// Called when the game starts or when spawned
	virtual void BeginPlay() override;

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = Trajectory)
	USplineComponent

	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Category = Trajectory)
	float ThrusterSpeed;
public:	
	// Called every frame
	virtual void Tick(float DeltaTime) override;

	virtual void Recalculate();
	virtual void QueueOrbit(class ABody* BodyToOrbit);
	
};
*/