// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "GameFramework/GameStateBase.h"
#include "AstralBazaarGameStateBase.generated.h"

/**
 * 
 */
UCLASS()
class ASTRALBAZAAR_API AAstralBazaarGameStateBase : public AGameStateBase
{
	GENERATED_BODY()

public:
	AAstralBazaarGameStateBase();

private:
	FDateTime SimulationDateTime;

public:
	virtual void Tick(float DeltaTime) override;

	UFUNCTION(BlueprintPure, Category = "Simulation Time")
	FORCEINLINE FDateTime GetSimulationDateTime() const;
};
