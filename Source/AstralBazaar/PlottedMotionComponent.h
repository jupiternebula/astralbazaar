// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#pragma once

#include "Components/ActorComponent.h"
#include "PlottedMotionComponent.generated.h"

UENUM(BlueprintType)
enum class EPlottedMotionLoopType : uint8
{	
	Clamped,     // Clamps the phase based on CycleLength
	Continuous,  // The simulation loops continuously taking the modulus of the phase and the PhaseLength
	Pulsating,   // Waits after clamping the phase and then loops the phase back to StartingPhase
	OpenEnded    // No clamping is applied. Any phase is accepted. Behavior can be undefined
};

class ADEPRECATED_PlottedMotionIndicator;

UCLASS(Deprecated, ClassGroup=PlottedMotion, abstract )
class ASTRALBAZAAR_API UDEPRECATED_PlottedMotionComponent : public UActorComponent
{
	GENERATED_BODY()

public:	
	// Sets default values for this component's properties
	UDEPRECATED_PlottedMotionComponent();

public:	
	// Called every frame
	virtual void TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction) override;
	virtual void OnRegister() override;
	virtual void PostEditChangeProperty(FPropertyChangedEvent&) override;

	// Should the motion repeat at the end of a complete cycle?
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlottedMotion)
	EPlottedMotionLoopType LoopType;

	UPROPERTY(EditInstanceOnly, BlueprintReadWrite, Category = PlottedMotion)
	uint8 bShowDebugHelpers : 1;

	// Should the AdvanceActor function (called in the Tick function) operate as an offset (useful when the motion is relative to another actor)? Otherwise, the actor's world location is used instead. TODO: This has not been implemented yet, always operates as an offset.
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = PlottedMotion)
	uint8 bAdvanceActorAsOffset : 1;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlottedMotion) // @TODO: Make this private and add bounds checking!
	float StartingPhase;

	// The duration of one cycle of simulation in seconds. Defaults to 1.0
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = PlottedMotion)
	float CycleDuration;

	// The maximum phase value possible in one cycle of the plotted motion. Defaults to 1.0
	UPROPERTY(EditDefaultsOnly, BlueprintReadWrite, Category = PlottedMotion)
	float CycleLength;

	// Returns the phase if a delta time were applied
	UFUNCTION(BlueprintPure, Category = PlottedMotion)
	FORCEINLINE float DeltaTimeToPhase(float DeltaTime) const;

	UFUNCTION(BlueprintPure, Category = PlottedMotion)
	virtual FVector PredictOffsetVectorAtPhase(float PredictedPhase) const { unimplemented(); return FVector(); }

	// Return a predicted offset vector. Useful when the plotted motion is relative to another actor (such as orbital motion)
	UFUNCTION(BlueprintPure, Category = PlottedMotion)
	FVector PredictOffsetVector(float DeltaTime, float& NewPhase) const;

	// Simply used as a shorthand in C++ in case you don't care about the new phase when calling PredictOffsetVector
	FVector PredictOffsetVector(float DeltaTime) const;

	UFUNCTION(BlueprintPure, Category = PlottedMotion)
	FORCEINLINE float GetCurrentPhase() const;

	/* @TODO: Implement later
	UFUNCTION(BlueprintCallable, Category = PlottedMotion)
	void SetCurrentPhase(float NewPhase);
	*/

	UFUNCTION(BlueprintCallable, Category = PlottedMotion)
	virtual float AdvanceActor(float DeltaTime);

	UFUNCTION(BlueprintCallable, Category = PlottedMotion)
	void Start();

	UFUNCTION(BlueprintCallable, Category = PlottedMotion)
	void Resume();

	UFUNCTION(BlueprintCallable, Category = PlottedMotion)
	void Stop();

	UFUNCTION(BlueprintCallable, Category = PlottedMotion)
	ADEPRECATED_PlottedMotionIndicator* SpawnIndicator(float AnimationTime, bool ScaleToCycleDuration);

protected:
	UPROPERTY(BlueprintReadOnly, VisibleAnywhere, Transient, Category = PlottedMotion)
	float CurrentPhase;

	UPROPERTY(BlueprintReadOnly, Transient, Category = PlottedMotion)
	float SecondsActive; // @TODO: This may need to be made into a double

	UPROPERTY(EditDefaultsOnly, Category = PlottedMotionIndicator)
	TSubclassOf<ADEPRECATED_PlottedMotionIndicator> IndicatorClass;

	virtual void CalculatePath() { unimplemented(); }

	UPROPERTY(BlueprintReadOnly, Transient, Category = PlottedMotion)
	uint8 bActive : 1;
};
