// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#include "AstralBazaar.h"
#include "PlottedMotionIndicator.h"
#include "PlottedMotionComponent.h"

// Sets default values
ADEPRECATED_PlottedMotionIndicator::ADEPRECATED_PlottedMotionIndicator()
{
	PrimaryActorTick.bCanEverTick = true;

	ParticleSystem = CreateDefaultSubobject<UParticleSystemComponent>(TEXT("ParticleSystem"));
	ParticleSystem->SetupAttachment(RootComponent);
}
/* WILL NOT DEPRECATE! WHAT IS WRONG WITH UNREAL!!!!!!!!!!!!!!!!!!!!!
void ADEPRECATED_PlottedMotionIndicator::VisualizePlottedMotionComponent_DEPRECATED(UDEPRECATED_PlottedMotionComponent* ComponentToVisualize, float AnimationTime, bool ScaleToCycleDuration)
{
	VisualizedPlottedMotionComponent_DEPRECATED = ComponentToVisualize;

	if (IsValid(ClonedPlottedMotionComponent_DEPRECATED))
	{
		ClonedPlottedMotionComponent_DEPRECATED->DestroyComponent();
	}

	ClonedPlottedMotionComponent_DEPRECATED = DuplicateObject(VisualizedPlottedMotionComponent_DEPRECATED, this,
		MakeUniqueObjectName(this, VisualizedPlottedMotionComponent_DEPRECATED->GetClass()));
	ClonedPlottedMotionComponent_DEPRECATED->RegisterComponent();

	ClonedPlottedMotionComponent_DEPRECATED->Stop();
	ClonedPlottedMotionComponent_DEPRECATED->StartingPhase = VisualizedPlottedMotionComponent_DEPRECATED->GetCurrentPhase();

	// @NOTE: May need to also ignore OpenEnded loop types
	if (ClonedPlottedMotionComponent_DEPRECATED->LoopType != EPlottedMotionLoopType::Continuous)
	{
		// @NOTE: Might need to && with FMath::NearlyEqual due to float inaccuracies
		// Destroy this actor if the visualized actor has reached its destination
		if (ClonedPlottedMotionComponent_DEPRECATED->StartingPhase >= ClonedPlottedMotionComponent_DEPRECATED->CycleLength)
			Destroy();

		ClonedPlottedMotionComponent_DEPRECATED->LoopType = EPlottedMotionLoopType::Pulsating;
	}

	// Set this actor's CustomTimeDilation based on the actor it is visualizing multiplied by the provided parameter to this function
	CustomTimeDilation = VisualizedPlottedMotionComponent_DEPRECATED->GetOwner()->CustomTimeDilation;

	ClonedPlottedMotionComponent_DEPRECATED->CycleDuration = ScaleToCycleDuration ? AnimationTime / VisualizedPlottedMotionComponent_DEPRECATED->CycleDuration : AnimationTime;

	ClonedPlottedMotionComponent_DEPRECATED->Start();
}
*/
void ADEPRECATED_PlottedMotionIndicator::BeginPlay()
{
	Super::BeginPlay();

}

// Called every frame
void ADEPRECATED_PlottedMotionIndicator::Tick(float DeltaTime)
{
	Super::Tick(DeltaTime);

	// @NOTE: May need to ignore continuous or open-ended loop types
	if (IsValid(ClonedPlottedMotionComponent_DEPRECATED))
	{
		ClonedPlottedMotionComponent_DEPRECATED->StartingPhase = VisualizedPlottedMotionComponent_DEPRECATED->GetCurrentPhase();
	}
}

