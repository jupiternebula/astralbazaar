// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.
/*
#pragma once

#include "GameFramework/Actor.h"
#include "PlottedSplinePath.generated.h"

UCLASS(Blueprintable, Abstract)
class ASTRALBAZAAR_API APlottedSplinePath : public AActor
{
	GENERATED_BODY()
	
public:	
	APlottedSplinePath();

	UPROPERTY(BlueprintReadOnly, EditAnywhere)
	class USplineComponent* SplineComponent;

	UPROPERTY(BlueprintReadWrite, EditAnywhere)
	uint16 NumSplinePoints;

protected:
	virtual void BeginPlay() override;

public:	
	virtual void Tick(float DeltaTime) override;
	
	UFUNCTION(BlueprintCallable, Category = PlottedSplinePath)
	virtual void CalculatePath() { unimplemented(); }
};
*/