// Copyright (C) 2017 Joshua William Taylor. All Rights Reserved.

#include "AstralBazaar.h"
#include "Body.h"
#include "AstralBazaarPlayerControllerBase.h"

AAB_PlayerControllerBase::AAB_PlayerControllerBase()
{

}

bool AAB_PlayerControllerBase::SelectBody_Implementation(ABody* BodyToSelect)
{
	bool Failed;

	SelectedBodies.Add(BodyToSelect, &Failed);
	BodyToSelect->OnSelect(this);
	BodySelectedDelegate.Broadcast(BodyToSelect, this);

	return !Failed;
}
/*
void AAB_PlayerControllerBase::SelectBodies_Implementation(TSet<ABody*> BodiesToSelect)
{
	
	return;
}
*/

bool AAB_PlayerControllerBase::UnselectBody_Implementation(ABody* BodyToUnselect)
{
	BodyToUnselect->OnUnselect(this);
	BodyUnselectedDelegate.Broadcast(BodyToUnselect, this);
	return (SelectedBodies.Remove(BodyToUnselect) > 0);
}
/*
void AAB_PlayerControllerBase::UnselectBodies_Implementation(TSet<ABody*> BodiesToUnselect)
{
	
	return;
}
*/
void AAB_PlayerControllerBase::UnselectAllBodies_Implementation()
{
	for (auto& UnselectedBody : SelectedBodies)
	{
		UnselectedBody->OnUnselect(this);
	}

	SelectedBodies.Empty(1);
	BodyUnselectedAllDelegate.Broadcast(this);
	return;
}

bool AAB_PlayerControllerBase::CheckSelection(ABody* BodyToQuery) const
{
	return SelectedBodies.Contains(BodyToQuery);
}

TSet<ABody*> AAB_PlayerControllerBase::GetSelectedBodies() const
{
	return SelectedBodies;
}