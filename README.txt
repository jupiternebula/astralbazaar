Astral Bazaar and its assets not attributed to other copyright owners are Copyright (C) 2017 Joshua William Taylor.

DEVELOPERS:
Please add the following to the bottom of the ".git/hooks/post-checkout" and ".git/hooks/post-commit" hooks:

# Update Unreal project version string in "Saved/Windows"
cd JupiterNebula/Scripts
./JN_UpdateProjectVersion.sh